import cv2
import pytesseract
import numpy as np
from tqdm import tqdm

file_name = input()
img = cv2.imread(file_name)
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
print(pytesseract.image_to_string(img, lang='rus', config='--psm 6'))
# cv2.imshow('', img)
# cv2.waitKey(0)
